<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('name');
            $table->string('category')->nullable();
            $table->boolean('published');
            $table->integer('order')->nullable();
            $table->timestamps();
            $table->text('price')->nullable();
            $table->text('description')->nullable();
            $table->text('specification')->nullable();
            $table->text('options')->nullable();
            $table->text('installation')->nullable();
            $table->text('price_detail')->nullable();
            $table->text('images')->nullable();
            $table->text('downloads')->nullable();
            $table->string('css_class')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
