const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('vendor/bower_components/bootswatch/paper', 'resources/assets/sass/paper')
    .copy('vendor/bower_components/bootstrap/dist/fonts', 'public/fonts')
    .copy('node_modules/flexslider/jquery.flexslider-min.js', 'public/js/jquery.flexslider-min.js')
    .copy('resources/assets/css/flexslider.css', 'public/css/flexslider.css')
    .copy('node_modules/flexslider/fonts', 'public/css/fonts')
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/installations.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
