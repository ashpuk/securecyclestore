<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class HomeController extends Controller
{

    public $news;

    public function __construct(NewsController $news)
    {
        $this->news = $news;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->news->newsList();
        foreach ($articles as $article) {
            $string = strip_tags($article->body);

            if (strlen($string) > 250) {

                // truncate string
                $stringCut = substr($string, 0, 250);

                // make sure it ends in a word so assassinate doesn't become ass...
                $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="/news">Read More</a>';
            }
            //dd($string);
            $article->body = $string;
        }
        return view('welcome', ['news' => $articles]);
    }

    public function landingPage($article)
    {
        $article = News::where('sef_url', $article)->first();
        return view('article', ['article' => $article]);
    }

}
