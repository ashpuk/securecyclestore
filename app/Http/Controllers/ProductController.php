<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Files;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Storage;
use App\Mail\DownloadBrochure;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{

    /*********************** COMMUNAL SCRIPTS **********************/

    public static function getCategories()
    {
        return Categories::all();
    }

    /*********************** CUSTOMER VIEWS ************************/


public function showPublishedProducts($selected_category)
    {
        $category = Categories::where('sef_url', $selected_category)->get()->first();
        $products = Product::where('category' , $category->id)->where('published', true)->orderBy('order', 'asc')->get();
        return view('products', ['category' => $category, 'products' => $products, 'meta_keywords' => $category->meta_keywords, 'meta_description' => $category->meta_description]);
    }


    public function showProduct($category, $product)
    {
        $product = Product::where('sef_url', $product)->get()->first();
        $images = Files::where('product_id', $product->id)
            ->where('type', '=', 'jpeg')->get();
        return view('productDetail', ['category' => $category, 'product' => $product, 'meta_keywords' => $product->meta_keywords, 'meta_description' => $product->meta_description, 'images' => $images]);
    }

    public function PDFProduct($product)
    {
        $product = Product::where('sef_url', $product)->get()->first();
        $images = Files::where('product_id', $product->id)
            ->where('type', '=', 'jpeg' )->get();
        $pdf = SnappyPdf::loadView('productPDF', ['product' => $product, 'images' => $images])
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0)
            ->setOption('margin-bottom', 0);
//            ->setOption('header-html', 'https://securecyclestore.com/pdf-header')
//            ->setOption('footer-html', 'https://securecyclestore.com/pdf-footer');
        return $pdf->download('product_' . $product->sef_url . '.pdf');
    }

    public function getScsBrochure(Request $request)
    {

        try {
            Mail::to('ash@eggshaped.co.uk')->send(new DownloadBrochure($request));
        } catch ( \Exception $e) {
            return back()->with('alert-danger', 'Oops, there was a problem, please call us.');
        }

        $file = storage_path() . '/app/scs-brochure.pdf';
//        $request->session()->flash('alert-success', 'Thanks for entering your details, your download should start immediately'); // todo cant do double response, would have to do a redirect, with session flashed and then get the page to do download depending on session content
        return response()->download($file);
    }

    /********************************* ADMIN VIEWS ***************************/

    public function showNewProduct()
    {
        $categories = Categories::pluck('name', 'name');
        return view('newProduct', ['categories' => $categories]);
    }

    public function createNewProduct(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
        ]);

        try
        {
            $product = new Product;
            $product->name = $request->name;
            $product->category = $request->category;
            $product->published = false;
            $product->save();
            return redirect('/admin/edit-product/' . $product->id)->with('alert-success', 'Product created');
        } catch ( \Exception $e) {
            return redirect('/admin/new-product')->with('alert-warning', 'Something went wrong; ' . $e->getMessage());
        }
    }

    public function editProduct($id)
    {
        $product = Product::where('id', $id)->get()->first();
        $categories = Categories::pluck('name', 'id');
        $files = Files::where('product_id', $id)->get();
        return view('editProduct', ['product' => $product, 'categories' => $categories, 'files' => $files]);
    }

    public function updateProduct(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
        ]);

        try
        {
            $published = ($request->published == 'published') ? true : false;
            $product = Product::where('id', $request->id)->get()->first();
            $product->name = $request->name;
            $product->category = $request->category;
            $product->published = $published;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->specification = $request->specification;
            $product->options = $request->options;
            $product->installation = $request->installation;
            $product->price_detail = $request->price_detail;
            $product->css_class = $request->class;
            $product->sef_url = $request->sef_url;
            $product->meta_keywords = $request->meta_keywords;
            $product->meta_description = $request->meta_description;
            $product->lead_time = $request->lead_time;
            $product->save();
            if ($request->submit == 'Update') {
                return redirect('/admin/edit-product/' . $request->id)->with('alert-success', 'Product updated');
            } else
            {
                return redirect('/admin')->with('alert-success', 'Product updated');
            }
        } catch ( \Exception $e) {
            return redirect('/admin/add-product')->with('alert-warning', 'Something went wrong; ' . $e->getMessage());
        }
    }



}
