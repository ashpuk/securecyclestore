<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{


    public function  allCategoriesList()
    {
        $news = News::all();
        return view('newsList', ['news' => $news]);
    }

    public function newsList()
    {
            return News::where('category', 'news')
                ->where('published', '=', '1')->get();
    }

    public function showNewsList()
    {
        $news = $this->newsList();
        return view('news', ['news' => $news]);
    }

    public function landingPageList()
    {
        return $landingPages = News::where('category', 'Landing Page')->get();
    }

    public function showArticle($id)
    {
        $article = News::where('id', $id)->first();
        return view('article', ['article' => $article, 'meta_keywords' => $article->meta_keywords, 'meta_description' => $article->meta_description]);
    }

}
