<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\News;
use Illuminate\Support\Facades\Auth;
use App\Categories;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
use Spatie\Sitemap\SitemapGenerator;


class AdminController extends Controller
{

    public function logout() {
        Auth::logout();
        return redirect('/')->with('alert-success', 'You have logged out');
    }

    /**
     * Show full product list for admin area
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function productList()
    {
            $products = Product::all();
            return view('admin', ['products' => $products]);
    }

    public function editArticle($id = null)
    {
        if ($id) {
            $article = News::where('id', $id)->get()->first();
        } else {
            $article = null;
        }
        return view('editArticle', ['article' => $article]);
    }

    public function updateArticle(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'category' => 'required',
        ]);


        try
        {
            $published = ($request->published == 'published') ? true : false;
            if (!$request->id) {
                $article = new News;
            } else {
                $article = News::where('id', $request->id)->get()->first();
            }
            $article->title = $request->title;
            $article->body = $request->body;
            $article->published = $published;
            $article->meta_title = $request->meta_title;
            $article->meta_keywords = $request->meta_keywords;
            $article->meta_description = $request->meta_description;
            $article->category = $request->category;
            $article->sef_url = $request->sef_url;
            $article->save();
            $this->sitemap();
            return redirect('/admin/news')->with('alert-success', 'Article added/updated!');
        } catch ( \Exception $e) {
            return redirect('/admin/edit-article')->with('alert-warning', 'Something went wrong; ' . $e->getMessage());
        }

    }

    public function categoryList()
    {
        $categories = Categories::all();
        return view('categoryList', ['categories' => $categories]);
    }

    public function editCategory($id = null)
    {
        if ($id) {
            $category = Categories::where('id', $id)->get()->first();
        } else {
            $category = null;
        }
        return view('editCategory', ['category' => $category]);
    }

    public function updateCategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);


        try
        {
            $published = ($request->published == 'published') ? true : false;
            if (!$request->id) {
                $category = new Categories;
            } else {
                $category = Categories::where('id', $request->id)->get()->first();
            }
            $category->name = $request->name;
            $category->intro = $request->intro;
            $category->published = $published;
            $category->meta_keywords = $request->meta_keywords;
            $category->meta_description = $request->meta_description;
            $category->sef_url = $request->sef_url;
            $category->save();
            $this->sitemap();
            return redirect('/admin/categories')->with('alert-success', 'Category added/updated!');
        } catch ( \Exception $e) {
            return redirect('/admin/edit-category')->with('alert-warning', 'Something went wrong; ' . $e->getMessage());
        }

    }

    public function contactForm(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'string|nullable',
        ]);

        try {
            Mail::to('ash@eggshaped.co.uk')->send(new Contact($request));
            return back()->with('alert-success', 'Thanks for your email, we will be in touch soon!');
        } catch ( \Exception $e) {
            return back()->with('alert-danger', 'Oops, there was a problem, please call us.');
        }
    }

    public function sitemap()
    {
        try
        {
            $path = 'sitemap.xml';
            SitemapGenerator::create('https://securecyclestore.com')->writeToFile($path);
            return back()->with('alert-success', 'Sitemap updated');
        } catch ( \Exception $e ) {
            return back()->with('alert-danger', 'Something went wrong: ' . $e->getMessage());
        }
    }
}
