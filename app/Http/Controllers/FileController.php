<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Files;
use Illuminate\Support\Facades\Storage;
use Image;

class FileController extends Controller
{
    protected function resizeProductImage($request, $file)
    {
        $path = "product/{$request->productId}/770x520_{$request->filename}";
        $resized = Image::make($file)
            ->resize(770,520, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        $resized->resizeCanvas(770,520, 'center');
        Storage::disk('images')->put($path, (string) $resized->encode());
        return 'images/' . $path;
    }

    protected function resizeProductThumb($request, $file)
    {
        $path = "product/{$request->productId}/370x208_{$request->filename}";
        $resized = Image::make($file)
            ->resize(370,208, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        $resized->resizeCanvas(370,208, 'center');
        Storage::put($path, (string) $resized->encode());
        return storage_path() . '/app/' . $path;
    }

    protected function saveFileToDb(Request $request, $original_filepath, $web_filepath = null, $thumb_filepath = null, $file)
    {

        $newFile = new Files;
        $newFile->filename = ($request->name ? $request->name . '.' . $file->getClientOriginalExtension() : $file->getClientOriginalName());
        $newFile->filepath = $original_filepath;
        $newFile->type = $file->guessClientExtension();
        $newFile->product_id = $request->productId;
        $newFile->caption = ($request->caption ? $request->caption : '');
        $newFile->web_image = $web_filepath;
        $newFile->thumb_image = $thumb_filepath;
        $newFile->save();
        return $newFile;
    }

    public function uploadFile(Request $request)
    {

        try{
            $file = request()->file('file');
            $original_filepath = "product/{$request->productId}/";
            request()->file('file')->store($original_filepath);
            $original_filepath = $original_filepath . $file->hashName();
            $filename = ($request->name ? $request->name . '.' . $file->getClientOriginalExtension() : $file->getClientOriginalName());
            $request->request->add(['filename' => $filename]);
            if (request()->file('file')->guessClientExtension() !== "pdf") {
                $web_filepath = $this->resizeProductImage($request, $file);
                $thumb_filepath = $this->resizeProductThumb($request, $file);
            }
            $fileInfo = $this->saveFileToDb($request, $original_filepath, $web_filepath, $thumb_filepath, $file);
            return response()->json(['file' => $fileInfo], 200);
        } catch (\Exception $e) {
            return response()->json(['file' => $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine()], 400);
        }
    }

    public function getProductFiles($id)
    {
        return Files::where('product_id', $id)->get();
    }

    public function deleteFile($id)
    {
        try {
            $image = Files::findorfail($id);
            $filepath = $image->filepath;
            $web_filepath = public_path() . '/' . $image->web_image;
            $thumb_filepath = strstr($image->thumb_image, 'product/');
            Storage::delete($filepath, $thumb_filepath);
            unlink($web_filepath);
            $image->delete();
            return response()->json(200);
        } catch ( \Exception $e) {
            return response()->json(['file' => $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine()], 400);
        }
    }

    public function makePrimary($id)
    {
        try {
            $image = Files::findorfail($id);
            $product_images = Files::where('product_id', $image->product_id)->get()->all();
            foreach($product_images as $product_image)
            {
                $product_image->primary = false;
                $product_image->save();
            }
            $image->primary = true;
            $image->save();
        } catch ( \Exception $e) {
            return response()->json(['file' => $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine()], 400);
        }
    }
}
