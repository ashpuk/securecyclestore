/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VeeValidate from 'vee-validate';
import {mapFields} from 'vee-validate';

Vue.use(VeeValidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./googleAnalytics');

Vue.component('example', require('./components/Example.vue'));
// Vue.component('simpleform', require('./components/SimpleForm.vue'));
var app = new Vue({

    el: '#app',

    template: `    <div class="simple-root form-horizontal">
        <div :class="{'form-group': true, 'has-warning': fields.email && fields.email.invalid, 'has-success': fields.email && fields.email.valid }">
            <label class="control-label col-sm-2">Email</label>
            <div class="col-sm-10">
                <input v-validate="'required|email'" v-model="email" :class="{'form-control': true}" name="email"
                       type="text" placeholder="Email">
                <span v-show="errors.has('email')" class="help has-warning">{{ errors.first('email') }}</span>
            </div>
        </div>
        <div :class="{'form-group': true, 'has-warning': fields.name && fields.name.invalid, 'has-success': fields.name && fields.name.valid}">
            <label class="control-label col-sm-2">Name</label>
            <div class="col-sm-10">
                <input v-validate="'required'" v-model="name" :class="{'form-control': true }" name="name"
                       type="text" placeholder="Name">
                <span v-show="fields.name && fields.name.invalid" class="help has-warning">{{errors.first('name') }}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button type="submit" class="btn btn-primary" :disabled="!formDirty">Download</button>
            </div>
        </div>
    </div>`,

    data: () => ({
        email: '',
        name: '',
    }),
    computed: {
        mapped: mapFields({
            nameFlags: 'name',
            emailFlags: 'email',
        }),
        formDirty() {
            // are some fields dirty?
            return Object.keys(this.fields).every(key => this.fields[key].valid);
        },
    }
});


const file = {
    template: `
    <tr>
        <td>{{ filename }}</td>
        <td>{{ type }}</td>
        <td>{{ caption }}</td>
        <td>{{ updated_at }}</td>
        <td><span v-if="primary == true" class="glyphicon glyphicon-ok"></span></td>
        <td><button @click="makePrimary(id)" class="btn btn-sm btn-primary">Main Image</button>
        <button @click="deleteFile(id)" class="btn btn-sm btn-default">Delete</button></td>
    </tr>
    `,

    props: ['filename', 'type', 'caption', 'updated_at', 'id', 'primary'],

    methods: {
        deleteFile(id) {
            axios.get('/admin/delete-file/' + id).then(response => {
                this.$parent.$emit('refreshAfterUpdate');
            })
                .catch(error => console.log(error));
        },
        makePrimary(id) {
            axios.get('/admin/make-primary/' + id).then(response => {
                this.$parent.$emit('refreshAfterUpdate');
            })
                .catch(error => console.log(error));
        },
    },
};

const table = {
    components: {file,},

    template: `
    <div><table class="table table-hover">
        <thead>
            <td>Filename</td>
            <td>File Type</td>
            <td>Caption</td>
            <td>Updated</td>
            <td>Main Image</td>
            <td>Actions</td>
        </thead>
        <tbody>
            <tr v-if="fileData == ''"><td colspan="6">There are no files</td></tr>
            <file v-for="file in fileData" :filename="file.filename" :type="file.type" :caption="file.caption" :updated_at="file.updated_at" :id="file.id" :primary="file.primary"></file>
        </tbody>
    </table>
        <h3>Upload File</h3>
        {{ message }}
    <div class="form-horizontal">
        <div class="form-group">
            <label for="file" class="col-lg-2 control-label">Select File</label>
            <div class="col-lg-10">
                <input id="file" class="form-control" name="file" type="file" />
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-lg-2 control-label">File Name (overwrite)</label>
            <div class="col-lg-10">
                <input class="form-control" name="name" type="text" v-model="name" />
            </div>
        </div>
        <div class="form-group">
            <label for="caption" class="col-lg-2 control-label">Caption (optional)</label>
            <div class="col-lg-10">
                <input class="form-control" name="caption" type="text" v-model="caption" />
            </div>
        </div>
        <button class="btn btn-default" @click="uploadFile">Submit</button>
    </div>
    </div>
   `,

    props: {
        table: {
            type: Object,
        },
    },

    data() {
        return {
            fileData: '',
            productId: '',
            name: '',
            caption: '',
            message: '',
        };
    },

    methods: {
        'refresh': function () {
            var productId = document.getElementsByName("id")[0].value;
            axios.get('/admin/get-product-files/' + productId)
                .then(response => {
                    this.fileData = response.data;
                });
            this.name = '';
            this.caption = '';
            this.message = '';
        },

        'uploadFile': function (name, caption) {
            if (document.getElementById('file').files[0]) {
                let formData = new FormData();
                formData.append('file', document.getElementById('file').files[0]);
                formData.append('name', this.name);
                formData.append('productId', document.getElementsByName("id")[0].value);
                formData.append('caption', this.caption);
                console.log(formData);
                axios.post('/admin/upload-file', formData).then(function (response) {
                    console.log('Server Responds with: ' + response.data);
                }).then(_ => {
                    this.refresh();
                })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                this.message = 'Please select a file';
            }
            ;
        },
    },

    mounted() {
        this.refresh();
        this.$on('refreshAfterUpdate', () => {
            this.refresh();
        })
    },
}

new Vue({
    render(createElement) {
        const props = {
            table: {
                type: Object,
            },
        };
        return createElement(table, {props});
    },
}).$mount("#files");