@extends('layouts.app')

@section('title', 'Installation')

@section('content')
    <div class="row">
        <div class="col-sm-8">
        <h1>Installation</h1>
        <p class="lead">The route we take</p>
        <p>At Secure Cycle Store, a dedicated contracts manager will oversee every aspect of your
            project from start to finish.</p>
        <p>Our experienced team will complete all necessary Risk Assessment Forms and ensure the
            installation process is completed to the highest quality, on time, and more
            importantly, to budget.</p>
        <p><strong>Installation options</strong></p>
        <ul>
            <li>Cycle Shelters</li>
            <li>Cycle Racks</li>
            <li>Cycle Sheds</li>
            <li>Accessories for cycle storage</li>
        </ul>
        <p>Any technical requirements you have or challenges you face - we can help.</p>
        <p>As well as having a core product range, we also provide a bespoke service allowing us
            to cater for any specific design, installation or budgetary requirements.</p>
        <p><a class="btn btn-primary" href="/contact">Get in touch</a></p>
            <br />
            <img class="img-responsive" src="/images/installation/shed_install.jpg" />
        </div>
        <div class="col-sm-4">
            <img class="img-responsive" src="/images/installation/installation.jpg" />
            <img class="img-responsive top-buffer" src="/images/installation/installation2.jpg" />
            <img class="img-responsive top-buffer" src="/images/installation/installation3.jpg" />
            <img class="img-responsive top-buffer bottom-buffer" src="/images/installation/installation4.jpg" />
        </div>
    </div>
@endsection