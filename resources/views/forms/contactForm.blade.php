            <div class="well bs-component">
                {!! Form::open(['url' => '/contact-form', 'class' => 'form-horizontal']) !!}
                <fieldset>
                    <legend>{{ $title }}</legend>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                            {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Your name (required)']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                            {!! Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Your email (required)']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('tel', 'Tel', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                            {!! Form::text('tel', '', ['class' => 'form-control', 'placeholder' => 'Your telephone number']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('message', 'Message', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                            {!! Form::textarea('message', '' , ['class' => 'form-control', 'rows' => '3']) !!}
                        </div>
                    </div>
                    {!! Form::hidden('source', $source) !!}
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                    data-placement="top" title=""
                                    data-original-title="Send message">Submit</button>
                        </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}
            </div>
