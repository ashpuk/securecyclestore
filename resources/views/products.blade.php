@extends('layouts.app')

@section('title', isset($category->name) ? $category->name : '')
@section('styles')
    <link href="/css/flexslider.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 well">
                <h1>{{ isset($category->name) ? $category->name : '' }}</h1>
                <div>{!! isset($category->intro) ? $category->intro : '' !!}</div>
            </div>
        </div>
        <div class="row">

        @foreach($products as $product)
                <div class="col-sm-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">{{ $product->name }}</div>
                        <div class="panel-body" style="height:auto !important;">
                            <div class="row">
                                <div class="col-sm-8 no-gutter">
                                    <div class="thumb-img">
                                        <a href="/product/{{ strtolower($category->sef_url) }}/{{ $product->sef_url }}"><img src="{{ $product->main_image }}" class="img-responsive" /></a>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <p><strong>Price: </strong>{{ $product->price }} <span class="text-muted">ex. VAT and devliery</span></p>
                                    <p><strong>Lead time: </strong>{{ $product->lead_time }}</p>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12 top-buffer" style="margin-bottom:15px;" >
                                    <div class="clearfix" >
                                        <a class="btn btn-default pull-right" href="/product/{{ strtolower($category->sef_url) }}/{{ $product->sef_url }}">{{ $product->name }} ></a><div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
            </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/jquery.flexslider-min.js"></script>
    @foreach($products as $product)
        <script>
            $(document).ready(function() {

                var {{ $product->css_class }}Slider = $('.{{ $product->css_class }}-slider').flexslider({
                    animation: "slide",
                    slideshowSpeed: 7000,
                    animationLoop: true,
                    slideshow: true,
                    directionNav: true,
                    controlNav: true,

                });

                {{ $product->css_class }}Slider.resize();

            });
        </script>
    @endforeach

@endsection