@extends('layouts.app')
@section('title', $article->name)
@section('content')
<div class="col-md-12">
    <h1>{{ $article->name }}</h1>
    <p>{!! $article->body !!}</p>
</div>
@endsection
@section('scripts')
@endsection