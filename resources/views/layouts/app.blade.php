<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Secure Cycle Store - @yield('title')</title>
    {!! isset($meta_keywords) ? '<meta name="keywords" content="' . $meta_keywords . '"/>' : null !!}
    {!! isset($meta_description) ? '<meta name="description" content="' . $meta_description . '"/>' : null !!}
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    @yield('styles')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="row">
                <div id="contact-info">
                    <div class="container">
                        <div class="col-sm-12">
                            <div class="glyphicon glyphicon-phone-alt"></div> 0151 933 8895    <div class="pull-right"><div class="glyphicon glyphicon-envelope"></div> <a href="mailto:sales@securecyclestore.com">sales@securecyclestore.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header heading">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" title="Cycle Storage Solutions"><img src="/images/SCS.png" height="50px"/></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse subheading teal" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (!Auth::guest())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/admin/categories">Edit Categories</a></li>
                                    <li><a href="/admin">Edit Products</a></li>
                                    <li><a href="/admin/news">Edit News</a></li>
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                            </li>
                        @endif
                        <li><a href="{{ url('/about-us') }}">About Us</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Product Range <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @include('layouts.category-list')
                            </ul>
                        </li>
                        <li><a href="/installation">Installation</a></li>
                        <li><a href="/news">News</a></li>
                        <li><a href="{{ url('/contact') }}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row">
            <div class="col-md-12">
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @yield('content')
</div>
<div id="footer" class="container-fluid well">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <a>
                    <li><a href="/breeam-ene9-compliant-cycle-storage" >BREEAM Ene9 Transport Cycle Storage</a></li>
                    <li><a href="/code-for-sustainable-homes-compliant-garden-cycle-sheds" >Code for Sustainable Homes Ene8 Cycle Storage</a></li>
                    <li><a href="/home-quality-mark-compliant-cycle-sheds" >Home Quality Mark (HQM) Garden Cycle Sheds</a></li>
                    <li><a href="/secured-by-design-compliant-garden-cycle-shed" >Secured by Design (SBD) Garden Cycle Shed</a></li>
                    <li><a href="/bike-stands-with-integrated-locking-systems">Bike Stands with Integrated Locking Systems</a></li>
                    </ul>
            </div>
            <div class="col-sm-4">
                <h6>Address</h6>
                <p>thecodestore.co.uk Ltd<br />44 Canal Street<br />Bootle<br />Liverpool<br />L20 8QU<br /><strong>Tel:</strong> 0151 933 8895<br /><strong>Email:</strong><a href="mailto:sales@thecodestore.co.uk"> sales@thecodestore.co.uk</a></p>
            </div>
            <div class="col-sm-4">
                <h6>Company Info</h6>
                <p>Secure Cycle Store is a trading name of <a href="http://thecodestore.co.uk">
                        thecodestore.co.uk Ltd</a></p>
                <p>All specifications, prices, and lead times displayed are subject to change and are provided as
                    a guide only. Please <a href="/contact">contact us</a> for a formal quotation.</p>
                <p>&#9400; {{ date('Y', strtotime("now")) }} thecodestore.co.uk Ltd (7055646) <span class="pull-right"><a href="/login">Admin Login</a></span> </p>
            </div>
        </div>
    </div>
</div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
{{--<script src="http://www.socialintents.com/api/chat/socialintents.1.1.js#2c9f82f45ad1aa8f015ad7f4d751063a" async="async"></script>--}}
@yield('scripts')

</body>
</html>
