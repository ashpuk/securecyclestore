@extends('layouts.app')
@section('title', 'Edit News')
@section('content')
<div class="col-md-12">
    <h1>Admin Area</h1>
    <h3>News List</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td>Article ID</td>
                <td>Title</td>
                <td>Published</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            @if($news == '[]')
                <tr>
                    <td colspan="4">There are no articles</td>
                </tr>
            @else
                @foreach($news as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td>{{ $article->title }}</td>
                    <td><input type="checkbox" name="published" {{ $article->published == 1 ? "checked" : "" }} disabled ></td>
                    <td><a href="/admin/edit-article/{{ $article->id }}">Edit</a> </td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>
    <p><a href="/admin/edit-article" class="btn btn-primary btn-lg">+ Add article</a></p>
</div>
@endsection
@section('scripts')
@endsection