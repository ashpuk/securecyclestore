@extends('layouts.app')
@section('title', 'Secure Cycle Storage solutions installed across the UK')
@section('styles')
<link href="/css/flexslider.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
            <div class="container">
                    <div class="welcome-slider flexslider">
                        <ul class="slides">
                            <li>
                                <img src="/images/slider/integrated-lock-header.jpg" alt="Bike stand with integrated lock."/>
                                <p class="flex-caption">Our exclusive range of bike stands with integrated locks for the ultimate convenience! No need to carry around a heavy lock with you any more. <a href="/bike-stands-with-integrated-locking-systems" title="Bike stand with integrated lock">Read more ></a></p>
                            </li>
                            <li>
                                <img src="/images/slider/corten-steel-bike-stand.jpg" alt="CorTen Steel Bike Stand" />
                                <p class="flex-caption">Select bike stands now available in CorTen weathered steel finish for a distinctive look. <a href="/product/cycle-racks/pedal-post" title="CorTen Steel Bike Stand">Read more ></a> </p>
                            </li>
                            <li>
                                <img src="/images/slider/cycle-storage-sub-contracting.jpg" alt="Cycle Storage installation and sub-contracting" />
                                <p class="flex-caption">Pictured: Installation at the prestigious Berkeley St. George development at Battersea Reach, London. <a href="/installation" title="Cycle Storage Sub-contracting and installation">Read more ></a></p>
                            </li>
                            <li>
                                <img src="/images/slider/cycle-shelters-header.jpg" alt="Secure Cycle Shelter"/>
                                <p class="flex-caption">Pictured: 'Trinity' FSC Timber Clad <a href="/products/cycle-shelters/">Secure Cycle Shelter</a>, designed in house to your requirements.</p>
                            </li>
                            <li>
                                <img src="/images/slider/secure-by-design-cycle-shed.jpg" alt="Secure by Design Garden cycle sheds" />
                                <p class="flex-caption">Secure by Design Secure <a href="/products/garden-cycle-sheds" title="Secure by Design Garden bike sheds">Garden Sheds</a> for cycles</p>
                            </li>
                        </ul>
                    </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">SECURE Cycle Shelters</div>
                            <img src="/images/thumbs/shelters/canal_curve_12_space_semi_vert_sliding_door.jpg" alt="Canal Curve Secure Cycle Shelter" class="img-responsive"/>
                            <div class="panel-body">
                                <p>A range of secure cycle shelters with mesh, perspex and FSC wooden clad finishes.</p>
                                <br>
                                <p><a class="btn btn-default pull-right" href="/products/cycle-shelters">Cycle Shelters ></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">SECURE Cycle Racks</div>
                            <img src="/images/thumbs/racks/semi_vertical_cycle_stand.jpg" alt="Semi Vertical Cycle Stand" class="img-responsive"/>
                            <div class="panel-body">
                                <p>A range of secure cycle racks for space saving secure cycle storage. Can contribute towards developers BREEAM Ene9 transport credits.</p>
                                <p><a class="btn btn-default pull-right" href="/products/cycle-racks-stands">Cycle Racks ></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">SECURE Cycle Garden Sheds</div>
                            <img src="/images/thumbs/sheds/secure_by_design_shed.jpg" class="img-responsive" alt="Secure by Design SBD Shed" class="img-responsive"/>
                            <div class="panel-body">
                                <p>Creators of the original ENE8 Code for Sustainable Homes (CfSH) Cycle Shed, complying with Secure by Design (SBD), and Home Quality Mark (HQM) criteria.</p>
                                <p><a class="btn btn-default pull-right" href="/products/garden-cycle-sheds">Garden Cycle Sheds ></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">SECURE Cycle Accessories</div>
                            <img src="/images/thumbs/accessories/silver_sold_secure_padlock.jpg" class="img-responsive" alt="Silver Sold Secure Padlock" />
                            <div class="panel-body">
                                <p>A selection of secure fixings including Sold Secure rated padlocks, ground anchors, and hasp and staples to keep your bike locked up.</p>
                                <p><a class="btn btn-default pull-right" href="/products/secure-cycle-accessories">Secure Cycle Fixings ></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">SECURE Cycle Two Tier Rack</div>
                            <img src="/images/thumbs/racks/two_tier_cycle_rack.jpg" class="img-responsive" alt="Two tier space saving cycle rack" />
                            <div class="panel-body">
                                <p>The most popular space saving solution for mass cycle storage - the two tier cycle rack.</p>
                                <br>
                                <a class="btn btn-default pull-right" href="/product/cycle-racks/two-tier-cycle-rack">Two Tier Cycle Racks ></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">Installation/Sub-contracting</div>
                            <img src="/images/thumbs/install/cycle_storage_sub_contractors_installers.jpg" class="img-responsive" alt="Cycle Storage Installers and Sub-contractors" />
                            <div class="panel-body">
                                <p>In house contracts manager and trained team of installers with CSCS cards, CITB training and NHBC Safemark accreditation.</p>
                                <p><a class="btn btn-default pull-right clearfix" href="/installation">Installation Team ></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="parallax"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <h2>Why choose <span><img src="/images/SCS.png" height="50px"/></span>?</h2>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-sm-1 big-icon glyphicon glyphicon-piggy-bank"></div>
                                <div class="col-sm-11"><h5>Cost-Effective</h5><p>Cost-effective prices and we guarantee an exceptional service.
                                    Your project will be overseen by a dedicated
                                    contracts manager who will provide a single point of contact from start
                                        to finish.</p></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-1 big-icon glyphicon glyphicon-lock"></div>
                                <div class="col-sm-11"><h5>Secure</h5><p>We offer a range of secure fixings
                                    with our cycle storage and where appropriate, our sheds can be made to
                                     the exact specifications outlined in the Seucred by Design New Homes 2016 criteria</p>
                                </div>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-sm-6">
                            <div class="col-sm-1 big-icon glyphicon glyphicon-cog"></div>
                                <div class="col-sm-11"><h5>Innovative</h5><p>Innovation is at the heart of what
                                we do. We were the first company in the UK to design and manufacture
                                a shed to meet the requirements for the Code for Sustainable Homes,
                                        Secured by Design (SBD) and the Home Quality Mark (HQM)</p></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-1 big-icon glyphicon glyphicon-tree-deciduous"></div>
                                <div class="col-sm-11"><h5>Sustainable</h5><p>Sustainability specialists for
                                    construction. All our products are 100 percent recyclable and the
                                    timber we use in our cycle storage is FSC certified to ensure it
                                        meets the highest environmental and social standards</p></div>
                            </div>
                        </div>
                        <div class="row top-buffer bottom-buffer">
                            <div class="col-sm-6">
                                <div class="col-sm-1 big-icon glyphicon glyphicon-globe"></div>
                                <div class="col-sm-11"><h5>National</h5><p>Nationwide delivery and short
                                        lead times as many of our products are held in stock</p></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-1 big-icon glyphicon glyphicon-user"></div>
                                <div class="col-sm-11"><h5>Skilled</h5><p>We have a highly skilled installation team which
                                        has undergone all necessary risk assessments and are CSCS registered</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="jumbotron">
                            <h1>Clients</h1>
                            <p>Some of the companies we have worked for include the biggest in the business</p>
                            <div class="flexslider clients">
                                <ul class="slides">
                                    <li>
                                        <img src="/images/clients/bloor_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/keepmoat_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/bellway_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/berkeley_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/wates_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/willmott_dixon_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/barratt_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/hill_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/redrow_homes_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/st_modwen_logo.png" />
                                    </li>
                                    <li>
                                        <img src="/images/clients/united_living_logo.png" />
                                    </li>
                                </ul>
                            </div>
                            <p><a href="/about-us" class="btn btn-primary btn-lg">About Us</a></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <h1>News</h1>
                        </div>
                        <div class="row">
                            @foreach($news as $article)
                                <div class="col-sm-6">
                                    <div class="panel panel-yellow">
                                        <div class="panel-heading">{{ $article->title }}<span class="pull-right text-muted">{{ date("j M Y", strtotime($article->updated_at)) }}</span></div>
                                        <div class="panel-body"><p></p>{!! $article->body !!}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

@endsection
@section('scripts')
    <script src="js/jquery.flexslider-min.js"></script>
    <script>
        $(document).ready(function() {

            $('.clients').flexslider({
                animation: "slide",
                itemWidth: 150,
                itemMargin: 80,
                move: 1,
                directionNav: false,
                controlNav: false,
                slideshowSpeed: 4000,
                animationLoop: true,
                slideshow: true,
                namespace: "client-",

            });

            $('.welcome-slider').flexslider({
                animation: "slide",
                slideshowSpeed: 7000,
                animationLoop: true,
                slideshow: true,
                directionNav: true,
                controlNav: true,

            });


        });
    </script>
    @endsection