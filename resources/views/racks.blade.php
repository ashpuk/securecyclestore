@extends('layouts.app')

@section('title', 'Page Title')
@section('styles')
    <link href="/css/flexslider.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 well">
            <h1>Secure Cycle Racks</h1>
            <p class="lead">Shift your cycle storage up a gear with the new Legends’ Range.</p>
            <p>The names behind these unique shelters have been inspired by some of
                our all-time favourite British cycling stars.  They are extremely
                versatile, available in three different sizes (6,12 and 18 bike),
                with various configurations.</p>
            <p>We also have a range of bike stand options and optional extras to
                choose from and will work with you to design a solution that best
                eets your specific project requirements.</p>
            <p><b>Cavendish Curve</b> - see separate attachment for full synopsis and description</p>
            <p><b>Wiggo Web</b> - see separate attachment for full synopsis and description</p>
            <p><b>Tip Trott Timber</b> - see separate attachment for full synopsis and description</p>
        </div>
    </div>
    @foreach($products as $product)
    <div class="row">
        <div class="page-header">
            <h2 class="">{{ $product->name }}</h2>
        </div>
        <div class="col-sm-8">
            <div class="">
                <div class="{{ $product->css_class }}-slider flexslider">
                    {!! $product->images !!}
                 </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Price</h3>
                </div>
                <div class="panel-body">
                    {!! $product->price !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#{{ $product->css_class }}-desc" data-toggle="tab">Description</a></li>
                <li><a href="#{{ $product->css_class }}-spec" data-toggle="tab">Specifications</a></li>
                <li><a href="#{{ $product->css_class }}-options" data-toggle="tab">Options</a></li>
                <li><a href="#{{ $product->css_class }}-inst" data-toggle="tab">Installation</a></li>
                <li><a href="#{{ $product->css_class }}-price" data-toggle="tab">Price</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Download <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        {!! $product->downloads !!}
                    </ul>
                </li>
            </ul>
            <div id="{{ $product->css_class }}-content" class="tab-content">
                <div class="tab-pane fade active in" id="{{ $product->css_class }}-desc">
                    {!! $product->description !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-spec">
                    {!! $product->specification !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-options">
                    {!! $product->options !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-inst">
                    {!! $product->installation !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-price">
                    {!! $product->price_detail !!}
                </div>
            </div>
        </div>
    </div>
    <hr />
@endforeach
</div>
@endsection
@section('scripts')
    <script src="/js/jquery.flexslider-min.js"></script>
    @foreach($products as $product)
    <script>
        $(document).ready(function() {

            var {{ $product->css_class }}Slider = $('.{{ $product->css_class }}-slider').flexslider({
                animation: "slide",
                slideshowSpeed: 7000,
                animationLoop: true,
                slideshow: true,
                directionNav: true,
                controlNav: true,

            });

            {{ $product->css_class }}Slider.resize();

        });
    </script>
    @endforeach

@endsection