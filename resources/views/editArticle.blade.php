@extends('layouts.app')

@section('title', 'Page Title')


@section('content')
<div class="col-sm-12">
    <div class="row">
        <div class="well bs-component">
            {!! Form::open(['url' => '/admin/edit-article', 'class' => 'form-horizontal']) !!}
            <fieldset>
                <legend>Add/edit an article</legend>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('title', isset($article) ? $article->title : '' , ['class' => 'form-control', 'placeholder' => 'Article Title']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('category', 'Category', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::select('category', ['News' => 'News', 'Landing Page' => 'Landing Page'], isset($article) ? $article->category : '' , ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('sef_url', 'SEF URL', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('sef_url', isset($article) ? $article->sef_url : '' , ['class' => 'form-control', 'placeholder' => 'whatever-you-want-to-display-in-the-url']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('meta_keywords', 'META Keywords', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('meta_keywords', isset($article) ? $article->meta_keywords : '' , ['class' => 'form-control', 'placeholder' => 'META Keywords']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('meta_description', 'META Description', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('meta_description', isset($article) ? $article->meta_description : '' , ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('body', 'Article', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('body', isset($article) ? $article->body : '' , ['class' => 'form-control', 'rows' => '3']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('published', 'Published', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::checkbox('published', 'published', isset($article->published) ? $article->published : '') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page">Save</button>
                        {{--<button class="btn btn-success" data-toggle="tooltip"--}}
                                {{--data-placement="top" title="saves changes"--}}
                                {{--data-original-title="Saves changes and reloads this page">Apply</button>--}}
                    </div>
                </div>
                {!! Form::hidden('id', isset($article) ? $article->id : null) !!}
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=2nwa2setkkz3w6v36ymgttoewpd2m74zdfqh98f39mvqy4g1"></script>
    <script>tinymce.init({
            selector: 'textarea',
            height: 250,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
    });</script>
@endsection
