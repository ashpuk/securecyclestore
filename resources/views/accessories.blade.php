@extends('layouts.app')

@section('title', 'Page Title')

@section('sidebar')
@parent

<p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <p>Trading since 2009 as <a href="http://www.thecodestore.co.uk">thecodestore.co.uk</a>,
    we have a wealth of experience installing cycle storage solutions on hundreds of sites across the UK.</p>
    <p>[show map with all of our installations on it]</p>
    <p>Previous clients include:</p>
    <p>[block of client logos]</p>
    <p>See our innovative product range <a href="/about">here</a></p>
    <p>Find out more about our installation team <a href="/installation">here</a></p>
@endsection