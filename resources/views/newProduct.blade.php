@extends('layouts.app')

@section('title', 'Edit product')


@section('content')
<div class="col-sm-12">
    <div class="row">
        <div class="well bs-component">
            {!! Form::open(['url' => '/admin/new-product', 'class' => 'form-horizontal']) !!}
            <fieldset>
                <legend>Create a product</legend>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', isset($product) ? $product->name : '' , ['class' => 'form-control', 'placeholder' => 'Product Name']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('category', 'Category', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::select('category', $categories, isset($product) ? $product->category : '' , ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" name="submit" class="btn btn-default" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Return to product list page">Return to Product List</button>
                        <button type="submit" name="submit" class="btn btn-success pull-right" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page" value="Update">Save</button>
                        <button type="submit" name="submit" class="btn btn-primary pull-right" style="margin-right:5px;" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page">Save & Return</button>
                    </div>
                </div>
                {!! Form::hidden('id', isset($product) ? $product->id : null) !!}
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=2nwa2setkkz3w6v36ymgttoewpd2m74zdfqh98f39mvqy4g1"></script>
    <script>tinymce.init({
            selector: 'textarea.tinymce',
            height: 250,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
    });</script>
@endsection
