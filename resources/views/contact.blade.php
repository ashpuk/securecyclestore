@extends('layouts.app')

@section('title', 'Contact Us')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <h1>Contact <img src="/images/SCS.png" height="70px" style="margin-top:-8px;margin-left:-12px"> </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <p>Secure Cycle Store is a trading name of thecodestore.co.uk Ltd.</p>
            <p><strong>Tel:</strong>0151 933 8895</p>
            <p><strong>Email:</strong><a href="mailto:sales@securecyclestore.com">sales@securecyclestore.com</a></p>
            <p><strong>Address:</strong>thecodestore.co.uk Ltd.<br />44 Canal Street<br />Bootle<br/>Liverpool<br/>L20 8QU</p>
        </div>
        <div class="col-sm-6">
        @include('forms.contactForm', ['title' => 'Got a question?', 'source' => 'Contact Page'])
        </div>
    </div>
@endsection