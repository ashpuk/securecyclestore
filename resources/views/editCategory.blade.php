@extends('layouts.app')

@section('title', 'Edit Category')


@section('content')
<div class="col-sm-12">
    <div class="row">
        <div class="well bs-component">
            {!! Form::open(['url' => '/admin/edit-category', 'class' => 'form-horizontal']) !!}
            <fieldset>
                <legend>Add/edit an category</legend>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', isset($category) ? $category->name : '' , ['class' => 'form-control', 'placeholder' => 'Category name']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('sef_url', 'SEF URL', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('sef_url', isset($category) ? $category->sef_url : '' , ['class' => 'form-control', 'placeholder' => 'SEF URL: secure-cycle-storage']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('intro', 'Introduction', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('intro', isset($category) ? $category->intro : '' , ['class' => 'form-control', 'rows' => '3']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('published', 'Published', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::checkbox('published', 'published', isset($category->published) ? $category->published : '') !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('meta_keywords', 'Meta Keywords', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('meta_keywords', isset($category) ? $category->meta_keywords : '' , ['class' => 'form-control', 'placeholder' => 'Meta Keywords e.g: cycle shed, cycle storage, secure by design shed etc']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('meta_description', 'Meta Description', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('meta_description', isset($category) ? $category->meta_description : '' , ['class' => 'form-control', 'placeholder' => 'Meta Description e.g: Secure by Design Cycle Shed made out of FSC Timber. ']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page">Save</button>
                        {{--<button class="btn btn-success" data-toggle="tooltip"--}}
                                {{--data-placement="top" title="saves changes"--}}
                                {{--data-original-title="Saves changes and reloads this page">Apply</button>--}}
                    </div>
                </div>
                {!! Form::hidden('id', isset($category) ? $category->id : null) !!}
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=2nwa2setkkz3w6v36ymgttoewpd2m74zdfqh98f39mvqy4g1"></script>
    <script>tinymce.init({
            selector: 'textarea',
            height: 250,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
    });</script>
@endsection
