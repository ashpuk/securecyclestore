@extends('layouts.app')
@section('title', 'About Us')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>About Us</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <p class="lead">Providing the route to secure sustainable cycle storage</p>
            <p>Secure Cycle Store is the new dedicated cycle storage range from
                <a href="http://www.thecodestore.co.uk">The Code Store.</a></p>
            <p>We are a specialist provider of cycle storage in residential, commercial and public
                sectors, with a focus on sustainability.</p>
            <p>With more than nine years’ industry experience, we have worked with some of the
                country’s leading house builders and construction companies, including Redrow Homes,
                Berkeley Homes, Kier Living, Wates Group, Bloor Homes, United Living, Keepmoat and
                Wilmott Dixon.</p>
            <p>Our portfolio boasts a range of communal shelters, bike shed and bike stand options
                and we provide secure locking points for wheels and frames.</p>
            <p>All our shelters and sheds are available in different sizes and configurations and
                we provide a bespoke service for any specific design requriements.</p>
            <p>Innovation is at the heart of what we do and we pride ourselves in providing secure
                space saving solutions to help you get the most out of your cycle storage. </p>
            <p><strong>We were the first company to design and
                    manufacture a shed to meet the requirements for the Code for Sustainable Homes,
                    Secured by Design (SBD) and the Home Quality Mark (HQM).</strong></p>
            <p>All our products are 100 percent recyclable and can be used to help demonstrate
                compliance with BREEAM and the Code for Sustainable Homes.</p>
            <p>The  timber we use in our cycle storage is Forrest Stewardship
                Council (FSC) certified to ensure it meets the highest environmental and social
                standards.</p>
            <p>All of our installation team are fully trained and experienced for working on
            construction site and hold the relevant CSCS cards and CITB training.</p>
        </div>
        <div class="col-md-2 col-md-offset-1 text-center">
            <img src="/images/breeam_cycle_storage_logo.png" alt="BREEAM Cycle Storage Logo" class="img-responsive top-buffer bottom-buffer">
            <img src="/images/fsc_cycle_storage.jpg" alt="FSC certified cycle storage and sheds" class="img-responsive top-buffer bottom-buffer">
            <img src="/images/citb_cscs_cycle_storage_installers.png" alt="CITB and CSCS trained cycle storage installers" class="img-responsive top-buffer bottom-buffer">
            <img src="/images/hqm_home_quality_mark_cycle_storage.jpg" alt="HQM Home Quality Mark Compliant Cycle Storage" class="img-responsive top-buffer">
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <h3>About our MD</h3>
            </div>
        </div>
    <div class="row">
        <div class="col-md-8">
                <p class="lead">A “wheelie” good guy who is passionate about cycling and construction Ashley
                 Prescott is the founder and MD of The Code Store which he set up in 2009.</p>
                <p>As a keen cyclist, himself, he works with developers and specifiers across the UK to
                    provide them with cycle storage solutions that allow them to build sustainably and
                    responsibly.</p>
                <p>“Anyone who has a bike knows that having somewhere safe and convenient to store it is
                    essential. I’m constantly getting told off by my wife for leaving mine in the hall
                    but the reality is I have nowhere else to put it.</p>
                <p>“Cycling has experienced a real boom in recent years and it is only going to get
                    more popular with the ongoing success of the British Cycling team. It’s a great way
                    of encouraging people to get healthy and exercise and is good for the environment.</p>
                <p>“It’s also a huge opportunity for developers as people want safe places to store
                    their precious wheels, whether it’s in their home, work or at the local rail station.</p>
                <p>“That’s where we can help. We are passionate about cycling and have created a
                    range of innovative, cost effective and space saving ideas that are ideal for
                    a host of residential and commercial applications.</p>
                <p>“Look at our product range, and if there is something you are looking for which
                    isn’t there- just ask. We offer a tailor-made service and can help you to find
                    the right fit for your specific project specifications.”</p>
            </div>
            <div class="col-md-4">
                <img src="/images/ash_bike.png" class="img-responsive"
                     alt="Director, Ashley Prescott competing in the Salford Triathlon"/>
            </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Our sites</h3>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <p>We have fulfilled a huge variety of cycle storage orders across the country. The map
                    shows where we have completed installations.</p>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div id="map" style="height: 300px;"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Our clients</h3>
                <div class="row vertical-align">
                    <div class="col-sm-2"><img src="/images/clients/wates_logo.png" class="img-responsive"></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/keepmoat_logo.png"
                                                               class="img-responsive"></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/bellway_logo.png"
                                                               class="img-responsive"/></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/willmott_dixon_logo.png"
                                                               class="img-responsive"></div>

                </div>
                <br/>
                <div class="row vertical-align">
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/hill_logo.png"
                                                               class="img-responsive"/></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/bloor_logo.png"
                                                               class="img-responsive"/></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/united_living_logo.png"
                                                               class="img-responsive"/></div>
                </div>
                <br/>
                <div class="row vertical-align bottom-buffer">
                    <div class="col-sm-2"><img src="/images/clients/barratt_logo.png" class="img-responsive"></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/redrow_homes_logo.png"
                                                               class="img-responsive"></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/berkeley_logo.png"
                                                               class="img-responsive"></div>
                    <div class="col-sm-2 col-sm-offset-1"><img src="/images/clients/st_modwen_logo.png"
                                                               class="img-responsive"/></div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="/js/installations.js"></script>
<script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9k8h_4QzM6VCbzfFaD1x7WysW8FJfMvc&callback=initMap">
</script>
@endsection