@extends('layouts.app')
@section('title', 'Edit Categories')
@section('content')
<div class="col-md-12">
    <h1>Admin Area</h1>
    <h3>Category List</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td>Category ID</td>
                <td>Title</td>
                <td>Published</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            @if($categories == '[]')
                <tr>
                    <td colspan="4">There are no categories</td>
                </tr>
            @else
                @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td><input type="checkbox" name="published" {{ $category->published == 1 ? "checked" : "" }} disabled ></td>
                    <td><a href="/admin/edit-category/{{ $category->id }}">Edit</a> </td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>
    <p><a href="/admin/edit-category" class="btn btn-primary btn-lg">+ Add category</a></p>
</div>
@endsection
@section('scripts')
@endsection