@extends('layouts.app')

@section('title', isset($product->name) ? $product->name : '')
@section('styles')
    <link href="/css/flexslider.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Secure Cycle Store Home</a></li>
                <li><a href="/products/{{ strtolower($category) }}">{{ ucfirst($category) }}</a></li>
                <li class="active">{{ $product->name }}</li>
            </ol>
        <div class="row">
            <div class="col-sm-12">
                <h2 class="">{{ $product->name }}</h2>
                <hr>
            </div>
                <div class="col-sm-8">
                    <div class="">
                        <div class="{{ $product->css_class }}-slider flexslider">
                            <ul class="slides">
                                @foreach($images as $image)
                                <li><img src="/{{  $image->web_image  }}" />
                                    <p class="flex-caption">{{ $image->caption }}</p>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Price</h3>
                        </div>
                        <div class="panel-body">
                            <p><strong>Price: </strong> {{ $product->price }} <span class="text-muted">ex. VAT and delivery</span></p>
                            <p><strong>Lead time: </strong> {{ $product->lead_time }}</p>
                        </div>
                    </div>
                    @include('forms.contactForm', ['title' => 'Fancy a quote?', 'source' => $product->name])

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#{{ $product->css_class }}-desc" data-toggle="tab">Description</a></li>
                        @if($product->specification)
                            <li><a href="#{{ $product->css_class }}-spec" data-toggle="tab">Specifications</a></li>
                        @endif
                        @if($product->options)
                            <li><a href="#{{ $product->css_class }}-options" data-toggle="tab">Options</a></li>
                        @endif
                        @if($product->installation)
                            <li><a href="#{{ $product->css_class }}-inst" data-toggle="tab">Installation</a></li>
                        @endif
                        @if($product->price_detail)
                            <li><a href="#{{ $product->css_class }}-price" data-toggle="tab">Price</a></li>
                        @endif
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Download <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/pdf/{!! $product->sef_url !!}" >Product Brochure</a></li>
                                <li><a data-toggle="modal" data-target=".modal">
                                        Download SCS Brochure
                                    </a></li>
                                @if($product->downloads){!! $product->downloads !!}@endif
                            </ul>
                        </li>
                    </ul>
                    <div id="{{ $product->css_class }}-content" class="tab-content">
                        <div class="tab-pane fade active in" id="{{ $product->css_class }}-desc">
                            {!! $product->description !!}
                        </div>
                        <div class="tab-pane fade" id="{{ $product->css_class }}-spec">
                            {!! $product->specification !!}
                        </div>
                        <div class="tab-pane fade" id="{{ $product->css_class }}-options">
                            {!! $product->options !!}
                        </div>
                        <div class="tab-pane fade" id="{{ $product->css_class }}-inst">
                            {!! $product->installation !!}
                        </div>
                        <div class="tab-pane fade" id="{{ $product->css_class }}-price">
                            {!! $product->price_detail !!}
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </div>
    <div class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Download</h4>
                </div>
                <div class="modal-body">
                    <p>Download the SECURE Cycle Store Brochure now.</p>
                    {!! Form::open(['action' => 'ProductController@getScsBrochure']) !!}
                    <div id="app">
                    </div>
                    {!! Form::hidden('source', $product->name) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/jquery.flexslider-min.js"></script>
        <script>
            $(document).ready(function() {
                var {{ $product->css_class }}Slider = $('.{{ $product->css_class }}-slider').flexslider({
                    animation: "slide",
                    slideshowSpeed: 7000,
                    animationLoop: true,
                    slideshow: true,
                    directionNav: true,
                    controlNav: true,
                });
                {{ $product->css_class }}Slider.resize();
            });
        </script>
@endsection