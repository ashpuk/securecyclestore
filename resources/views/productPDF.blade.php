<!DOCTYPE html>
<html>
<head>
    <link href="{{ public_path() . '/css/app.css' }}" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0; padding: 0;">
    <div id="pdf">
        <div id="pdf-header">
            <div class="container">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <img src="/home/forge/securecyclestore.com/public/images/SCS.png" height="50px"/>
                        </div>
                        <div class="col-xs-6">
                            <div class="text-muted pull-right">T: 0151 933 8895 | E: sales@securecyclestore.com</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="pdf-body" style="background: rgba(0,0,0,0.05); padding: 5px; margin: 0;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div><h1>{{ $product->name }}</h1></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        @if($product->description)<div><h3>Description</h3></div>
                        <div><p>{!! $product->description !!}</p></div>@endif
                        @if($product->specification)<div><h3>Specification</h3></div>
                        <div><p>{!! $product->specification !!}</p></div>@endif
                        @if($product->options)<div><h3>Options</h3></div>
                        <div><p>{!! $product->options !!}</p></div>@endif
                        @if($product->installation)<div><h3>Installation</h3></div>
                        <div><p>{!! $product->installation !!}</p></div>@endif
                        @if($product->price_detail)<div><h3>Pricing</h3></div>
                        <div><p>{!! $product->price_detail !!}</p></div>@endif
                    </div>
                    <div class="col-xs-6">
                        @if($images)
                            @foreach($images as $image)
                                <div><img src="{!! $image->thumb_image !!}" class="img-responsive" /><br /></div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>




