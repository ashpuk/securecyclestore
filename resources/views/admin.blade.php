@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
<div class="col-md-12">
    <h1>Admin Area</h1>
    <a href="/admin/generate-sitemap" class="btn btn-default pull-right">Update Sitemap</a>
    <h3>Product List</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td>Product ID</td>
                <td>Category</td>
                <td>Name</td>
                <td>Published</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            @if($products == '[]')
                <tr>
                    <td colspan="5">There are no products</td>
                </tr>
            @else
                @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->category }}</td>
                    <td>{{ $product->name }}</td>
                    <td><input type="checkbox" name="published" {{ $product->published == 1 ? "checked" : "" }} disabled ></td>
                    <td><a href="/admin/edit-product/{{ $product->id }}">Edit</a> </td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>
    <p><a href="/admin/new-product" class="btn btn-primary btn-lg">+ Add product</a></p>
</div>
@endsection
@section('scripts')
@endsection