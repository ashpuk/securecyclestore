@extends('layouts.app')

@section('title', 'Add/edit product')


@section('content')
<div class="col-sm-12">
    <div class="row">
        <div class="well bs-component">
            {!! Form::open(['url' => '/admin/add-product', 'class' => 'form-horizontal']) !!}
            <fieldset>
                <legend>Add a new product</legend>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" name="submit" class="btn btn-default" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Return to product list page">Return to Product List</button>
                        <button type="submit" name="submit" class="btn btn-success pull-right" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page" value="Update">Save</button>
                        <button type="submit" name="submit" class="btn btn-primary pull-right" style="margin-right:5px;" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page">Save & Return</button>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', isset($product) ? $product->name : '' , ['class' => 'form-control', 'placeholder' => 'Product Name']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('class', 'CSS Class', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('class', isset($product) ? $product->css_class : '' , ['class' => 'form-control', 'placeholder' => 'CSS Class prefix e.g. cav-']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('sef_url', 'SEF URL', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('sef_url', isset($product) ? $product->sef_url : '' , ['class' => 'form-control', 'placeholder' => 'Search Engine Friendly URL e.g. product-name-here']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('category', 'Category', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::select('category', $categories, isset($product) ? $product->category : '' , ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('published', 'Published', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::checkbox('published', 'published', isset($product) ? $product->published : '') !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('lead_time', 'Lead time', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('lead_time', isset($product) ? $product->lead_time : '' , ['class' => 'form-control', 'placeholder' => 'e.g: from two weeks']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('meta_keywords', 'Meta Keywords', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('meta_keywords', isset($product) ? $product->meta_keywords : '' , ['class' => 'form-control', 'placeholder' => 'Meta Keywords e.g: cycle shed, cycle storage, secure by design shed etc']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('meta_description', 'Meta Description', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('meta_description', isset($product) ? $product->meta_description : '' , ['class' => 'form-control', 'placeholder' => 'Meta Description e.g: Secure by Design Cycle Shed made out of FSC Timber. ']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('price', 'Price Box', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('price', isset($product) ? $product->price : '' , ['class' => 'form-control tinymce', 'rows' => '3', 'placeholder' => 'e.g: from £1,000']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('description', isset($product) ? $product->description : '' , ['class' => 'form-control tinymce', 'rows' => '3', 'placeholder' => 'Description Tab']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('specification', 'Specification', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('specification', isset($product) ? $product->specification : '' , ['class' => 'form-control tinymce', 'rows' => '3', 'placeholder' => 'Specification Tab']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('options', 'Options', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('options', isset($product) ? $product->options : '' , ['class' => 'form-control tinymce', 'rows' => '3', 'placeholder' => 'Options Tab']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('installation', 'Installation', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('installation', isset($product) ? $product->installation : '' , ['class' => 'form-control tinymce', 'rows' => '3', 'placeholder' => 'Installation Tab']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('price_detail', 'Price', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('price_detail', isset($product) ? $product->price_detail : '' , ['class' => 'form-control tinymce', 'rows' => '3', 'placeholder' => 'Price Tab']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('main_image', 'Main Image', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('main_image', isset($product) ? $product->main_image : '' , ['class' => 'form-control', 'placeholder' => 'path to main image for category view']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('images', 'Images', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('images', isset($product->images) ? $product->images : '<ul class="slides">
                        <li>
                            <img src="/images/renders/cav-semi-12-door.jpg" />
                        <p class="flex-caption">Cavendish Curve Shelter with semi-vertical bike rack with space for 12 bikes, including lockable sliding door</p>
                        </li>
                        <li>
                            <img src="/images/renders/cav-semi-12.jpg" />
                            <p class="flex-caption">Cavendish Curve Shelter with semi-vertical bike rack with space for 12 bikes</p>
                        </li>
                            etc....
                    </ul>' , ['class' => 'form-control', 'rows' => '12', 'placeholder' => 'Images (raw HTML)']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('downloads', 'Downloads', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('downloads', isset($product) ? $product->downloads : '' , ['class' => 'form-control', 'rows' => '3', 'placeholder' => 'Downloads (raw HTML)']) !!}
                        <span class="help-block">
                            &lt;li&gt;&lt;a href="#/downloads/filename.pdf"&gt;Footprint Plan&lt;/a&gt;&lt;/li&gt;<br />
                            &lt;li&gt;&lt;a href="#/downloads/filename.pdf"&gt;Brochure&lt;/a&gt;&lt;/li&gt;</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" name="submit" class="btn btn-default" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Return to product list page">Return to Product List</button>
                        <button type="submit" name="submit" class="btn btn-success pull-right" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page" value="Update">Save</button>
                        <button type="submit" name="submit" class="btn btn-primary pull-right" style="margin-right:5px;" data-toggle="tooltip"
                                data-placement="top" title=""
                                data-original-title="Save changes and back to product list page">Save & Return</button>
                    </div>
                </div>
                {!! Form::hidden('id', isset($product) ? $product->id : null) !!}
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=2nwa2setkkz3w6v36ymgttoewpd2m74zdfqh98f39mvqy4g1"></script>
    <script>tinymce.init({
            selector: 'textarea.tinymce',
            height: 250,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
    });</script>
@endsection
