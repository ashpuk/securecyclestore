@extends('layouts.app')
@section('title', 'News')
@section('styles')
<link href="/css/flexslider.css" rel="stylesheet" type="text/css">
@endsection
@section('content')

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <h2>Secure Cycle Storage News</h2>
                        </div>
                        @foreach($news as $article)
                            <h3>{{ $article->title }}</h3>
                            <p>{!! $article->body !!}</p>
                            <hr />
                        @endforeach
                    </div>
                </div>
            </div>

@endsection
@section('scripts')

@endsection