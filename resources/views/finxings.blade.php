@extends('layouts.app')

@section('title', 'Page Title')
@section('styles')
    <link href="/css/flexslider.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 well">
            <h1>Shelters</h1>
            <p class="lead">Shift your cycle storage up a gear with the new Legends’ Range.</p>
            <p>The names behind these unique shelters have been inspired by some of
                our all-time favourite British cycling stars.  They are extremely
                versatile, available in three different sizes (6,12 and 18 bike),
                with various configurations.</p>
            <p>We also have a range of bike stand options and optional extras to
                choose from and will work with you to design a solution that best
                eets your specific project requirements.</p>
            <p><b>Cavendish Curve</b> - see separate attachment for full synopsis and description</p>
            <p><b>Wiggo Web</b> - see separate attachment for full synopsis and description</p>
            <p><b>Tip Trott Timber</b> - see separate attachment for full synopsis and description</p>
        </div>
    </div>
    <!--
    <div class="row">
        <div class="page-header">
            <h2 class="">Cavendish Curve</h2>
        </div>
        <div class="col-sm-8">
            <div class="">
                <div class="cav-slider flexslider">
                    <ul class="slides">
                        <li>
                            <img src="/images/renders/cav-semi-12-door.jpg" />
                            <p class="flex-caption">Cavendish Curve Shelter with semi-vertical bike rack with space for 12 bikes, including lockable sliding door</p>
                        </li>
                        <li>
                            <img src="/images/renders/cav-semi-12.jpg" />
                            <p class="flex-caption">Cavendish Curve Shelter with semi-vertical bike rack with space for 12 bikes</p>
                        </li>
                        <li>
                            <img src="/images/renders/cav-toast-12.jpg" />
                            <p class="flex-caption">Cavendish Curve Shelter with toast rack with space for 12 bikes</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Price</h3>
                </div>
                <div class="panel-body">
                    <p>Price: from £1,000 <span class="text-muted">ex. VAT & delivery</span></p>
                    <p>Lead time: from 2 weeks</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#cav-desc" data-toggle="tab">Description</a></li>
                <li><a href="#cav-spec" data-toggle="tab">Specifications</a></li>
                <li><a href="#cav-options" data-toggle="tab">Options</a></li>
                <li><a href="#cav-inst" data-toggle="tab">Installation</a></li>
                <li><a href="#cav-price" data-toggle="tab">Price</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Download <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#dropdown1">Footprint Plan</a></li>
                        <li><a href="#dropdown2">Brochure</a></li>
                    </ul>
                </li>
            </ul>
            <div id="cav-content" class="tab-content">
                <div class="tab-pane fade active in" id="cav-desc">
                    <p>When it comes to cycle storage, make your “mark” with this classic
                        curved shelter. It’s part of our Legends Range which has been
                        inspired by some of our all-time favourite British cycling stars.</p>
                    <p>It is extremely versatile and is available in three sizes (6,12
                        and 18 bike) with various configurations. The single units can
                        also be placed alongside or opposite each other in order to form
                        a larger compound configuration.</p>
                    <p>It is an ideal storage solution for a whole host of applications
                        including railway stations, schools, housing, hospitals and universities.</p>
                    </p>
                </div>
                <div class="tab-pane fade" id="cav-spec">
                    <ul>
                        <li>Manufactured from a 50x 50 steel hollow section to BSEN ISO1461</li>
                        <li>Transparent PET mm roof with optional side panels or sliding doors</li>
                        <li>2.5 wall thickness cold rolled steel hollow section</li>
                        <li>Anti-climb end panels</li>
                        <li>No crawl under perimeters</li>
                        <li>Galvanized finish or powder coated with RAL colours available on request.</li>
                        <li>The Cavendish Curve can help contribute towards gaining ENE8 Cycle
                            storage credits when used in the appropriate situation, under the 
                            Code for Sustainable Homes.</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="cav-options">
                    <p>Floor fixings:</p>
                    <ul>
                        <li>Base plate fixing</li>
                        <li>Submerged fixing</li>
                    </ul>
                    Optional extras:
                    <ul>
                        <li>Sheffield Stands</li>
                        <li>Toast Rack</li>
                        <li>Semi-vertical</li>
                        <li>Helmet locker</li>
                        <li>Lighting</li>
                        <li>Security</li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="cav-inst">
                    <p>Minimum of 300mm thick concrete is required to safely secure structure.</p>
                    <p>All necessary fixings and fasteners needed for assembly are supplied (this includes floor fixings).</p>
                    <p>Installation cost: <a href="#" class="btn btn-info btn-xs">Contact us</a></p>
                </div>
                <div class="tab-pane fade" id="cav-price">
                    <p>From: £1,000</p>
                    <p>Lead time: from 2 weeks</p>
                    <p><a href="#" class="btn btn-info btn-xs">Get a Quote</a></p>
                </div>
            </div>
        </div>
    </div>
    <hr /> -->
    @foreach($products as $product)
    <div class="row">
        <div class="page-header">
            <h2 class="">{{ $product->name }}</h2>
        </div>
        <div class="col-sm-8">
            <div class="">
                <div class="{{ $product->css_class }}-slider flexslider">
                    {!! $product->images !!}
                    {{--<ul class="slides">--}}
                        {{--<li>--}}
                            {{--<img src="/images/renders/cav-semi-12-door.jpg" />--}}
                            {{--<p class="flex-caption">Cavendish Curve Shelter with semi-vertical bike rack with space for 12 bikes, including lockable sliding door</p>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<img src="/images/renders/cav-semi-12.jpg" />--}}
                            {{--<p class="flex-caption">Cavendish Curve Shelter with semi-vertical bike rack with space for 12 bikes</p>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<img src="/images/renders/cav-toast-12.jpg" />--}}
                            {{--<p class="flex-caption">Cavendish Curve Shelter with toast rack with space for 12 bikes</p>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Price</h3>
                </div>
                <div class="panel-body">
                    {!! $product->price !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#{{ $product->css_class }}-desc" data-toggle="tab">Description</a></li>
                <li><a href="#{{ $product->css_class }}-spec" data-toggle="tab">Specifications</a></li>
                <li><a href="#{{ $product->css_class }}-options" data-toggle="tab">Options</a></li>
                <li><a href="#{{ $product->css_class }}-inst" data-toggle="tab">Installation</a></li>
                <li><a href="#{{ $product->css_class }}-price" data-toggle="tab">Price</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Download <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        {!! $product->downloads !!}
                        {{--<li><a href="#dropdown1">Footprint Plan</a></li>--}}
                        {{--<li><a href="#dropdown2">Brochure</a></li>--}}
                    </ul>
                </li>
            </ul>
            <div id="{{ $product->css_class }}-content" class="tab-content">
                <div class="tab-pane fade active in" id="{{ $product->css_class }}-desc">
                    {!! $product->description !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-spec">
                    {!! $product->specification !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-options">
                    {!! $product->options !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-inst">
                    {!! $product->installation !!}
                </div>
                <div class="tab-pane fade" id="{{ $product->css_class }}-price">
                    {!! $product->price_detail !!}
                </div>
            </div>
        </div>
    </div>
    <hr />
@endforeach
</div>
@endsection
@section('scripts')
    <script src="/js/jquery.flexslider-min.js"></script>
    @foreach($products as $product)
    <script>
        $(document).ready(function() {

            var {{ $product->css_class }}Slider = $('.{{ $product->css_class }}-slider').flexslider({
                animation: "slide",
                slideshowSpeed: 7000,
                animationLoop: true,
                slideshow: true,
                directionNav: true,
                controlNav: true,

            });

            {{ $product->css_class }}Slider.resize();

        });
    </script>
    @endforeach

@endsection