<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function() {
    return View::make('pdfFooter');
});

Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin', 'AdminController@productList');
    Route::get('/admin/new-product', 'ProductController@showNewProduct');
    Route::post('/admin/new-product', 'ProductController@createNewProduct');
    Route::get('/admin/edit-product/{id}', 'ProductController@editProduct');
    Route::post('/admin/edit-product', 'ProductController@updateProduct');
    Route::get('/admin/get-product-files/{id}', 'FileController@getProductFiles');
    Route::get('/admin/delete-file/{id}', 'FileController@deleteFile');
    Route::post('/admin/upload-file', 'FileController@uploadFile');
    Route::get('/admin/make-primary/{id}', 'FileController@makePrimary');

    Route::get('/admin/news', 'NewsController@allCategoriesList');
    Route::get('/admin/edit-article/{id?}', 'AdminController@editArticle');
    Route::post('/admin/edit-article', 'AdminController@updateArticle');

    Route::get('/logout', 'AdminController@logout');

    Route::get('/admin/categories', 'AdminController@categoryList');
    Route::get('/admin/edit-category/{id?}', 'AdminController@editCategory');
    Route::post('/admin/edit-category', 'AdminController@updateCategory');

    Route::get('/admin/generate-sitemap', 'AdminController@sitemap');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', 'HomeController@index');

Route::get('/about-us', function () {
    return view('about');
});

Route::get('/products/{category}', 'ProductController@showPublishedProducts');
Route::get('/product/{category}/{product}', 'ProductController@showProduct');
Route::get('/pdf/{product}', 'ProductController@PDFProduct');
Route::post('/brochure', 'ProductController@getScsBrochure');
Route::get('/pdf-header', function() {
   return view('pdfHeader');
});
Route::get('/pdf-footer',  function() {
    return view('pdfFooter');
});

Route::get('/installation', function () {
    return view('installation');
});
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/news', 'NewsController@showNewsList');
Route::post('/contact-form', 'AdminController@contactForm');


Route::get('/{article}', 'HomeController@landingPage');




