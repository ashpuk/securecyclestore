/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 41);
/******/ })
/************************************************************************/
/******/ ({

/***/ 13:
/***/ (function(module, exports) {

window.initMap = function () {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 5,
    center: { lat: 52.633584, lng: -1.691032 },
    mapTypeId: 'hybrid'
  });

  // Create an array of alphabetical characters used to label the markers.
  var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given locations array.
  // The map() method here has nothing to do with the Google Maps API.
  var markers = locations.map(function (location, i) {
    return new google.maps.Marker({
      position: location,
      label: labels[i % labels.length]
    });
  });

  // Add a marker clusterer to manage the markers.
  var markerCluster = new MarkerClusterer(map, markers, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
};
var locations = [{ lat: 49.205707, lng: -2.179959 }, { lat: 50.445081, lng: -3.712571 }, { lat: 50.459841, lng: -4.262592 }, { lat: 50.660383, lng: -3.546517 }, { lat: 50.700943, lng: -1.92313 }, { lat: 50.712142, lng: -2.463642 }, { lat: 50.751405, lng: -3.385554 }, { lat: 50.759087, lng: -1.287172 }, { lat: 50.769997, lng: -1.546673 }, { lat: 50.777694, lng: -1.907999 }, { lat: 50.797727, lng: -0.680858 }, { lat: 50.801169, lng: -1.063954 }, { lat: 50.811234, lng: -1.072084 }, { lat: 50.817417, lng: -0.454842 }, { lat: 50.817764, lng: -0.433011 }, { lat: 50.825106, lng: -0.139987 }, { lat: 50.825639, lng: -0.533075 }, { lat: 50.826349, lng: -1.055074 }, { lat: 50.83049, lng: -0.392739 }, { lat: 50.83357, lng: -0.154732 }, { lat: 50.835085, lng: -0.181969 }, { lat: 50.838683, lng: -0.113413 }, { lat: 50.840399, lng: -0.170385 }, { lat: 50.842389, lng: -1.068101 }, { lat: 50.842493, lng: -1.067587 }, { lat: 50.844264, lng: -1.067538 }, { lat: 50.847205, lng: -0.213148 }, { lat: 50.847493, lng: -1.069917 }, { lat: 50.851646, lng: -0.169533 }, { lat: 50.858421, lng: -0.146946 }, { lat: 50.85878, lng: -1.399103 }, { lat: 50.859379, lng: -1.046137 }, { lat: 50.86258, lng: -1.286675 }, { lat: 50.874356, lng: -1.000507 }, { lat: 50.912101, lng: -1.403439 }, { lat: 50.916843, lng: -0.996937 }, { lat: 50.921377, lng: -1.608813 }, { lat: 50.930096, lng: -1.451347 }, { lat: 50.945327, lng: -1.222822 }, { lat: 50.961805, lng: -0.553455 }, { lat: 50.964343, lng: -1.299088 }, { lat: 50.978529, lng: -1.349713 }, { lat: 51.00528, lng: -0.107004 }, { lat: 51.00846, lng: -0.088044 }, { lat: 51.02402, lng: -0.353288 }, { lat: 51.03392, lng: -0.43968 }, { lat: 51.060925, lng: -0.324607 }, { lat: 51.061198, lng: -0.325525 }, { lat: 51.072169, lng: 1.129515 }, { lat: 51.07923, lng: -0.804649 }, { lat: 51.079932, lng: -1.336287 }, { lat: 51.088144, lng: -0.319469 }, { lat: 51.101908, lng: -0.399242 }, { lat: 51.111166, lng: -0.191114 }, { lat: 51.122305, lng: -0.84229 }, { lat: 51.136804, lng: 0.266634 }, { lat: 51.175402, lng: -0.014234 }, { lat: 51.196509, lng: -0.607874 }, { lat: 51.225383, lng: -0.342732 }, { lat: 51.231969, lng: -0.664427 }, { lat: 51.241442, lng: 0.548534 }, { lat: 51.253044, lng: -2.505875 }, { lat: 51.253897, lng: -0.935972 }, { lat: 51.256676, lng: -0.531924 }, { lat: 51.258086, lng: -0.931848 }, { lat: 51.25827, lng: -0.542178 }, { lat: 51.263933, lng: 1.08745 }, { lat: 51.27662, lng: 0.186504 }, { lat: 51.277436, lng: -1.107915 }, { lat: 51.282102, lng: -0.839941 }, { lat: 51.299397, lng: 0.578155 }, { lat: 51.304229, lng: -0.294416 }, { lat: 51.315081, lng: -0.559362 }, { lat: 51.334856, lng: -2.906421 }, { lat: 51.337611, lng: 0.539493 }, { lat: 51.348899, lng: -1.975053 }, { lat: 51.352795, lng: -0.060201 }, { lat: 51.358619, lng: -0.182819 }, { lat: 51.361812, lng: -1.162064 }, { lat: 51.367518, lng: -0.179048 }, { lat: 51.376989, lng: -0.210672 }, { lat: 51.379167, lng: -0.222615 }, { lat: 51.392579, lng: -0.295531 }, { lat: 51.401667, lng: -0.846968 }, { lat: 51.410751, lng: -0.835308 }, { lat: 51.411694, lng: 0.124577 }, { lat: 51.416079, lng: -0.257223 }, { lat: 51.418265, lng: -2.668056 }, { lat: 51.420035, lng: -0.693297 }, { lat: 51.423785, lng: -0.207105 }, { lat: 51.425232, lng: -0.219404 }, { lat: 51.427102, lng: -2.863757 }, { lat: 51.42776, lng: -0.165247 }, { lat: 51.432925, lng: -0.513277 }, { lat: 51.435381, lng: -0.223887 }, { lat: 51.435658, lng: -0.95145 }, { lat: 51.440981, lng: -3.194169 }, { lat: 51.442618, lng: -1.010486 }, { lat: 51.445339, lng: -0.325528 }, { lat: 51.446729, lng: 0.062276 }, { lat: 51.449483, lng: -0.963107 }, { lat: 51.449623, lng: -2.593856 }, { lat: 51.45533, lng: 0.013954 }, { lat: 51.455498, lng: -0.926068 }, { lat: 51.461659, lng: -0.041667 }, { lat: 51.46194, lng: -3.590956 }, { lat: 51.462421, lng: -0.074112 }, { lat: 51.464733, lng: -0.184049 }, { lat: 51.474118, lng: -0.060314 }, { lat: 51.474947, lng: -0.244166 }, { lat: 51.478097, lng: -3.271343 }, { lat: 51.479118, lng: -0.01468 }, { lat: 51.480483, lng: -0.839043 }, { lat: 51.482071, lng: -3.21549 }, { lat: 51.486052, lng: -2.612274 }, { lat: 51.486906, lng: -2.546986 }, { lat: 51.490483, lng: 0.161385 }, { lat: 51.491262, lng: -0.956268 }, { lat: 51.491571, lng: -0.163536 }, { lat: 51.497091, lng: -0.097979 }, { lat: 51.504004, lng: -3.19609 }, { lat: 51.504231, lng: -1.122441 }, { lat: 51.505817, lng: -0.24347 }, { lat: 51.509699, lng: -0.422963 }, { lat: 51.51418, lng: -0.139092 }, { lat: 51.52141, lng: -0.463191 }, { lat: 51.522092, lng: -0.104231 }, { lat: 51.525506, lng: -0.084109 }, { lat: 51.52799, lng: -0.13899 }, { lat: 51.529885, lng: -0.185278 }, { lat: 51.531176, lng: -0.073389 }, { lat: 51.536655, lng: -0.113965 }, { lat: 51.536833, lng: -0.113308 }, { lat: 51.547487, lng: -0.468024 }, { lat: 51.549892, lng: -0.181728 }, { lat: 51.561169, lng: -1.818999 }, { lat: 51.562263, lng: -0.00666 }, { lat: 51.564644, lng: 0.096147 }, { lat: 51.567698, lng: -1.475408 }, { lat: 51.576298, lng: 0.011951 }, { lat: 51.578255, lng: -0.017477 }, { lat: 51.579028, lng: -1.751203 }, { lat: 51.59347, lng: -0.143331 }, { lat: 51.594455, lng: 0.043622 }, { lat: 51.594463, lng: 0.62214 }, { lat: 51.595141, lng: -0.176485 }, { lat: 51.595177, lng: -3.183137 }, { lat: 51.59613, lng: 0.666322 }, { lat: 51.614253, lng: -0.410488 }, { lat: 51.616074, lng: 0.29922 }, { lat: 51.625683, lng: 0.038668 }, { lat: 51.62576, lng: -0.063649 }, { lat: 51.631965, lng: 0.005989 }, { lat: 51.633189, lng: -3.945936 }, { lat: 51.633227, lng: -0.191803 }, { lat: 51.63701, lng: -0.748543 }, { lat: 51.642943, lng: -0.104296 }, { lat: 51.643534, lng: 0.083668 }, { lat: 51.644946, lng: -0.363583 }, { lat: 51.652477, lng: -3.800581 }, { lat: 51.664284, lng: -0.170225 }, { lat: 51.669241, lng: -1.289496 }, { lat: 51.694241, lng: -3.423583 }, { lat: 51.698274, lng: -4.939417 }, { lat: 51.712894, lng: 0.501516 }, { lat: 51.714963, lng: 0.166586 }, { lat: 51.722178, lng: 0.499263 }, { lat: 51.726096, lng: -1.548357 }, { lat: 51.729098, lng: -2.363547 }, { lat: 51.743417, lng: -2.210412 }, { lat: 51.755684, lng: -3.225562 }, { lat: 51.780269, lng: -3.165215 }, { lat: 51.78458, lng: -1.279133 }, { lat: 51.798049, lng: -3.215443 }, { lat: 51.806808, lng: -3.870913 }, { lat: 51.829152, lng: 0.784095 }, { lat: 51.845508, lng: 0.94618 }, { lat: 51.849056, lng: -1.356717 }, { lat: 51.877784, lng: 0.552827 }, { lat: 51.902467, lng: -0.002129 }, { lat: 51.90495, lng: -2.07686 }, { lat: 51.909139, lng: -2.078205 }, { lat: 51.957284, lng: -0.648922 }, { lat: 51.993435, lng: -3.237463 }, { lat: 52.018562, lng: 0.279184 }, { lat: 52.046951, lng: -0.830262 }, { lat: 52.049173, lng: -0.691039 }, { lat: 52.050141, lng: 1.137946 }, { lat: 52.052106, lng: -0.69282 }, { lat: 52.052106, lng: -0.69282 }, { lat: 52.106921, lng: 1.100876 }, { lat: 52.128415, lng: -0.513074 }, { lat: 52.155548, lng: -0.191138 }, { lat: 52.182893, lng: 0.186233 }, { lat: 52.192144, lng: -2.190174 }, { lat: 52.240866, lng: 0.709736 }, { lat: 52.341841, lng: 0.508936 }, { lat: 52.438023, lng: -1.467272 }, { lat: 52.462778, lng: 1.566024 }, { lat: 52.488362, lng: -2.130746 }, { lat: 52.492892, lng: 1.241195 }, { lat: 52.497064, lng: -1.99947 }, { lat: 52.576108, lng: -2.097245 }, { lat: 52.593995, lng: -0.720453 }, { lat: 52.599143, lng: -1.666762 }, { lat: 52.608089, lng: 1.725476 }, { lat: 52.648392, lng: -1.701552 }, { lat: 52.759494, lng: -1.20294 }, { lat: 52.892619, lng: -1.494047 }, { lat: 52.986455, lng: -2.131935 }, { lat: 53.046024, lng: -3.001868 }, { lat: 53.139557, lng: 0.319004 }, { lat: 53.151095, lng: -3.140404 }, { lat: 53.167122, lng: -4.319394 }, { lat: 53.256654, lng: -2.123593 }, { lat: 53.282971, lng: -3.794661 }, { lat: 53.328685, lng: -1.651305 }, { lat: 53.347823, lng: -2.998342 }, { lat: 53.349725, lng: -2.77778 }, { lat: 53.359273, lng: -2.997497 }, { lat: 53.381363, lng: -2.154608 }, { lat: 53.389006, lng: -2.578304 }, { lat: 53.395687, lng: -2.290473 }, { lat: 53.401329, lng: -2.288899 }, { lat: 53.402303, lng: -2.319348 }, { lat: 53.406898, lng: -2.826613 }, { lat: 53.408148, lng: -1.517723 }, { lat: 53.410906, lng: -2.935249 }, { lat: 53.411738, lng: -2.29243 }, { lat: 53.418979, lng: -2.875855 }, { lat: 53.444973, lng: -2.995311 }, { lat: 53.455281, lng: -2.715719 }, { lat: 53.465993, lng: -2.261427 }, { lat: 53.47067, lng: -2.964961 }, { lat: 53.560316, lng: -2.660592 }, { lat: 53.614396, lng: -2.601576 }, { lat: 53.668535, lng: -2.974169 }, { lat: 53.71744, lng: -2.749516 }, { lat: 53.819491, lng: -2.755453 }, { lat: 53.839399, lng: -2.579373 }, { lat: 53.839847, lng: -1.725704 }, { lat: 53.975369, lng: -1.534283 }, { lat: 54.194682, lng: -3.075696 }, { lat: 54.231854, lng: -1.340977 }, { lat: 54.268288, lng: -2.973675 }, { lat: 54.315654, lng: -5.723899 }, { lat: 54.332847, lng: -1.44103 }, { lat: 54.350436, lng: -1.45093 }, { lat: 54.546045, lng: -1.550086 }, { lat: 54.546174, lng: -1.581048 }, { lat: 54.561408, lng: -1.179469 }, { lat: 54.574859, lng: -0.982641 }, { lat: 54.598584, lng: -1.438528 }, { lat: 54.705125, lng: -1.568829 }, { lat: 54.989614, lng: -1.486457 }];

/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(13);


/***/ })

/******/ });